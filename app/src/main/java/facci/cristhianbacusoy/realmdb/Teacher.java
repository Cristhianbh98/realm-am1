package facci.cristhianbacusoy.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Teacher extends RealmObject {
  @Required
  private String name;
  @Required
  private String lastName;
  @Required
  private String registerDate;
  @Required
  private String cellphone;

  public Teacher(String _name, String _lastName, String _registerDate, String _cellphone) {
    this.name = _name;
    this.lastName = _lastName;
    this.registerDate = _registerDate;
    this.cellphone = _cellphone;
  }

  public Teacher() {}

  public String getName() {
    return this.name;
  }

  public String getLastName() {
    return this.lastName;
  }
}
