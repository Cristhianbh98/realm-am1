package facci.cristhianbacusoy.realmdb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import org.bson.Document;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.mongodb.App;
import io.realm.mongodb.AppConfiguration;
import io.realm.mongodb.Credentials;
import io.realm.mongodb.User;
import io.realm.mongodb.mongo.MongoClient;
import io.realm.mongodb.mongo.MongoCollection;
import io.realm.mongodb.mongo.MongoDatabase;

public class MainActivity extends AppCompatActivity {
  Realm uiThreadRealm;
  MongoDatabase mongoDatabase;
  MongoClient mongoClient;
  String App_ID = "application-0-hhwog";
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Realm.init(this);

    // Open a Realm
    String realmName = "Teachers";
    RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
    uiThreadRealm = Realm.getInstance(config);

    addChangeListenerToRealm(uiThreadRealm);

    FutureTask<String> Task = new FutureTask(new BackgroundQuickStart(), "test");
    ExecutorService executorService = Executors.newFixedThreadPool(2);
    executorService.execute(Task);

    // Log in Realm
    App app = new App(new AppConfiguration.Builder(App_ID).build());

    Credentials credentials = Credentials.emailPassword("cristhianbacusoy@gmail.com", "rocks12");

    app.loginAsync(credentials, new App.Callback<User>() {
      @Override
      public void onResult(App.Result<User> result) {
        if (result.isSuccess()) {
          Log.v("user log", "Logged in with email");
        } else {
          Log.v("user log", "Logged failed");
        }
      }
    });

    User user = app.currentUser();
    mongoClient = user.getMongoClient("teachers");
    mongoDatabase = mongoClient.getDatabase("movilapp");
    MongoCollection<Document> mongoCollection = mongoDatabase.getCollection("teachers");

    Realm backgroundThreadRealm = Realm.getInstance(config);

    RealmResults<Teacher> teachers = backgroundThreadRealm.where(Teacher.class).findAll();

    for (Teacher teacher : teachers) {
      Log.e("Teachers", "Name: " + teacher.getName() + " " + teacher.getLastName());

      // Insert
      mongoCollection.insertOne(new Document("name", teacher.getName()).append("last_name", teacher.getLastName())).getAsync(result -> {
        if (result.isSuccess()){
          Log.v("DataInsert", "Datos Insertados correctamente");
        }else{
          Log.v("DataInsert", "Error: " + result.getError().toString());
        }
      });
    }
  }

  private void addChangeListenerToRealm(Realm realm) {
    // all Tasks in the realm
    RealmResults<Teacher> teachers = uiThreadRealm.where(Teacher.class).findAllAsync();
    teachers.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Teacher>>() {
      @Override
      public void onChange(RealmResults<Teacher> collection, OrderedCollectionChangeSet changeSet) {
        /*
          // process deletions in reverse order if maintaining parallel data structures so indices don't change as you iterate
          OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
          for (OrderedCollectionChangeSet.Range range : deletions) {
            Log.e("QUICKSTART", "Deleted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
          }
         */

        OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
        for (OrderedCollectionChangeSet.Range range : insertions) {
          Log.e("QUICKSTART", "Inserted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
        }

        OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
        for (OrderedCollectionChangeSet.Range range : modifications) {
          Log.e("QUICKSTART", "Updated range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
        }
      }
    });
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    uiThreadRealm.close();
  }

  public class BackgroundQuickStart implements Runnable {
    @Override
    public void run() {
      String realmName = "Teachers";
      RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
      Realm backgroundThreadRealm = Realm.getInstance(config);

      Teacher teacher1 = new Teacher("Cristhian Jossué", "Bacusoy Holguín", "19-12-2021", "059845122");
      Teacher teacher2 = new Teacher("Benito Alex", "Fernandez Lucas", "10-04-2020", "024512410" );
      Teacher teacher3 = new Teacher("Diego Raul", "Mera Palma", "05-12-1989", "025245120");
      Teacher teacher4 = new Teacher("Antonio Alexander", "Ferreira Holguin", "02-10-1999", "024512410" );

      backgroundThreadRealm.executeTransaction (transactionRealm -> {
        transactionRealm.insert(teacher1);
        transactionRealm.insert(teacher2);
        transactionRealm.insert(teacher3);
        transactionRealm.insert(teacher4);
      });


      backgroundThreadRealm.close();
    }
  }
}